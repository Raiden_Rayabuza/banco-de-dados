CREATE DATABASE Livraria
USE Livraria
--TABELAS
CREATE TABLE Livro(
	Codigo_Livro	INT				IDENTITY(1001,1),
	Nome			VARCHAR(100),
	Lingua			VARCHAR(100)	DEFAULT('PT-BR'),
	Ano				INT				CHECK (Ano >= 1990),
	PRIMARY KEY(Codigo_Livro)		
);
CREATE TABLE Edicoes(
	ISBN			INT,
	Pre�o			DECIMAL(7,2)	CHECK (Pre�o > 0),
	Ano				INT				CHECK (Ano >= 1993),
	Num_Paginas		INT				CHECK (Num_Paginas > 0),
	QTD_Estoque		INT,
	PRIMARY KEY(ISBN)
);
CREATE TABLE Editoras(
	Codigo_Editoras	INT				IDENTITY(1,1),
	Nome			VARCHAR(50)		UNIQUE,
	Logradouro		VARCHAR(255),
	Numero			INT				CHECK (Numero > 0),
	CEP				CHAR(8),
	Telefone		CHAR(11),
	PRIMARY KEY(Codigo_Editoras)
);
CREATE TABLE Autores(
	Codigo_Autor	INT		IDENTITY(10001,1),
	Nome			VARCHAR(100)	UNIQUE,
	Nascimento		DATE,
	Pais			VARCHAR(50)		CHECK(Pais = 'Brasil' OR Pais = 'Alemanha'),
	Biografia		VARCHAR(MAX),
	PRIMARY KEY(Codigo_Autor)
);
--ASSOCIATIVAS
CREATE TABLE Livro_Edicoes_Editoras(
	EdicoesISBN				INT		NOT NULL	FOREIGN KEY REFERENCES Edicoes(ISBN),
	EditoraCodigo_Editora	INT		NOT NULL	FOREIGN KEY REFERENCES Editoras(Codigo_Editoras),
	LivroCodigo_Livro		INT		NOT NULL	FOREIGN KEY REFERENCES Livro(Codigo_Livro),
);

CREATE TABLE Livro_Autor(
	AutorCodigo_Autor		INT		NOT NULL	FOREIGN KEY REFERENCES Autores(Codigo_Autor),
	LivroCodigo_Livro		INT		NOT NULL	FOREIGN KEY REFERENCES Livro(Codigo_Livro),
);

ALTER TABLE Autores
 ALTER COLUMN Nascimento VARCHAR(10);
ALTER TABLE Autores
 ALTER COLUMN Nascimento INT;

--1. O nome do autor � unico
INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('In�cio da Silva',1975,'Brasil','Programador WEB desde 1995')
INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('In�cio da Silva',1975,'Brasil','Programador WEB desde 1995')
--2. S� s�o permitidos autores do Brasil e da Alemanha
INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('LULA',1975,'EUA','Programador WEB desde 1995')
--3. A lingua padr�o dos l�vros � PT-BR
INSERT INTO Livro (Nome, Ano) VALUES ('CCNA 4.1',2015)
SELECT * FROM Livro
--4. N�o s�o cadastrados livros de ano inferior a 1990
INSERT INTO Livro (Nome, Ano) VALUES ('CCNA 4.1',1989)
--5. N�o s�o cadastrados edi�oes de ano inferior a 1993
INSERT INTO Edicoes (ISBN, Pre�o, Ano, Num_Paginas, QTD_Estoque) VALUES (0130661023,189.90,1992,653,10)
--6. Pre�o de edi��o n�o pode ser negativo
INSERT INTO Edicoes (ISBN, Pre�o, Ano, Num_Paginas, QTD_Estoque) VALUES (0130661023,-189.90,2018,653,10)
--7. Numero de paginas da edi��o n�o pode ser negativo
INSERT INTO Edicoes (ISBN, Pre�o, Ano, Num_Paginas, QTD_Estoque) VALUES (0130661023,189.90,2018,-653,10)
--8. Numero de endere�o da editora n�o pode ser negativo
INSERT INTO Editoras(Numero) VALUES (-1)
--9. Nome da editora � unico
INSERT INTO Editoras(Nome) VALUES ('Panini')