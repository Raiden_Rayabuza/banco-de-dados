--EX1
DECLARE @l1 INT,
		@l2 INT,
		@l3 INT,
		@igual INT

SET @l1 = 5
SET @l2 = 11
SET @l3 = 33
SET @igual = 0
IF(@l1 = 0 OR @l2 = 0 OR @l3 = 0)
	BEGIN
		RAISERROR('Um dos lados do tri�ngulo est� com 0', 16,1)
	END
ELSE
BEGIN
	IF(@l1 > (@l2 + @l3) OR @l2 > (@l1 + @l3) OR @l3 > (@l2 + @l1))
		BEGIN
			RAISERROR('Nenhum lado do tri�ngulo pode ser maior que a soma de dois lados', 16,1)
		END
	ELSE
		BEGIN
			IF(@l1 = @l2)
				BEGIN
					SET @igual += 1
				END
			IF(@l2 = @l3)
				BEGIN
					SET @igual += 1
				END
			IF(@l3 = @l1)
				BEGIN
					SET @igual += 1
				END
			IF(@igual = 0)
				BEGIN
					PRINT('� um tri�ngulo escaleno')
				END
			IF(@igual = 1)
				BEGIN
					PRINT('� um tri�ngulo isoceles')
				END
			IF(@igual = 3)
				BEGIN
					PRINT('� um tri�ngulo equil�tero')
				END
		END
END

--EX2

DECLARE @n1 INT,
		@n2 INT,
		@resul INT,
		@count INT
SET @n1 = 0
SET @n2 = 1
SET @resul = 0
SET @count = 0

WHILE(@count < 15)
	BEGIN
		SET @resul = @n1 + @n2
		PRINT(@resul + ' ')
		SET @n1 = @n2
		SET @n2 = @resul
		SET @count += 1
	END

--EX3
DECLARE @palavra VARCHAR(MAX),
		@palavra_inversa VARCHAR(MAX)
DECLARE @contador INT,
		@tam INT
SET @palavra = 'Amor a Roma'
SET @contador = LEN(@palavra)
SET @palavra_inversa = ''
WHILE(@contador >= 0)
	BEGIN
		SET @palavra_inversa += SUBSTRING(@palavra,@contador,1)
		SET @contador -= 1
	END
IF(@palavra = @palavra_inversa)
	BEGIN
		PRINT('Palavra � um palidromo')
		PRINT('Palavra Original: ' + @palavra)
		PRINT('Palavra Invertida: ' + @palavra_inversa)
	END
ELSE
	BEGIN
		PRINT('Palavra n�o � um palidromo')
		PRINT('Palavra Original: ' + @palavra)
		PRINT('Palavra Invertida: ' + @palavra_inversa)
	END

--EX4

DECLARE @frase VARCHAR(MAX)
DECLARE @quant_letra INT,
		@quant_palavra INT,
		@tam_palavra INT
SET @frase = 'Hoje � Sexta-feira'
SET @quant_letra = LEN(@frase)
SET @tam_palavra = 0
SET @quant_palavra = 0
WHILE(@tam_palavra <= @quant_letra)
	BEGIN
		IF(SUBSTRING(@frase,@tam_palavra,1) = ' ')
		BEGIN
			SET @quant_palavra += 1 
		END
		SET @tam_palavra += 1
	END
PRINT('Frase: ' + @frase)
PRINT('Quantidade de letras: ' + CAST(@quant_letra AS VARCHAR(MAX)))
PRINT('Quantidade de palavras: ' + CAST(@quant_palavra AS VARCHAR(MAX)))