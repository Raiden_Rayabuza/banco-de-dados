CREATE DATABASE Palestras
GO
USE Palestras


CREATE TABLE Curso(
	codigo_curso INT NOT NULL,
	nome_curso VARCHAR(70) NOT NULL,
	sigla VARCHAR(70) NOT NULL
	PRIMARY KEY (codigo_curso)
);

CREATE TABLE Aluno(
	ra CHAR(7) NOT NULL,
	nome_aluno VARCHAR(250) NOT NULL,
	codigo_curso INT NOT NULL,
	PRIMARY KEY(ra),
	FOREIGN KEY(codigo_curso) REFERENCES Curso(codigo_curso) 
);

CREATE TABLE Palestrante(
	codigo_palestrante INT NOT NULL IDENTITY(1,1),
	nome_palestrante VARCHAR(250) NOT NULL,
	empresa VARCHAR(100) NOT NULL,
	PRIMARY KEY(codigo_palestrante)
);

CREATE TABLE Palestra(
	codigo_palestra INT NOT NULL IDENTITY(1,1),
	titulo VARCHAR(MAX) NOT NULL,
	carga_horaria INT NULL,
	data DATETIME NOT NULL,
	codigo_palestrante INT NOT NULL,
	PRIMARY KEY (codigo_palestra),
	FOREIGN KEY (codigo_palestrante) REFERENCES Palestrante(codigo_palestrante)
);

CREATE TABLE Nao_Alunos(
	rg VARCHAR(9) NOT NULL,
	orgao_exp CHAR(5) NOT NULL,
	nome VARCHAR(250) NOT NULL,
	PRIMARY KEY (rg,orgao_exp)
);

CREATE TABLE Nao_Alunos_Inscritos(
	codigo_palestra INT NOT NULL,
	rg VARCHAR(9) NOT NULL,
	orgao_exp CHAR(5) NOT NULL,
	FOREIGN KEY (codigo_palestra) REFERENCES Palestra(codigo_palestra),
	FOREIGN KEY (rg,orgao_exp) REFERENCES Nao_Alunos(rg,orgao_exp),
);

CREATE TABLE Alunos_Inscritos(
	ra CHAR(7) NOT NULL,
	codigo_palestra INT NOT NULL,
	FOREIGN KEY (ra) REFERENCES Aluno(ra),
	FOREIGN KEY (codigo_palestra) REFERENCES Palestra(codigo_palestra)
);


CREATE VIEW V_Lista_De_Presen�a_Alunos
as
	SELECT al.ra AS num_documento, nome_aluno AS nome, titulo as titulo_palestra, nome_palestrante, carga_horaria, data
	FROM Aluno al 
	INNER JOIN Alunos_Inscritos alu_i ON al.ra = alu_i.ra 
	INNER JOIN Palestra pa ON pa.codigo_palestra = alu_i.codigo_palestra
	INNER JOIN Palestrante pales ON pales.codigo_palestrante = pa.codigo_palestrante

CREATE VIEW V_Lista_De_Presen�a__Nao_Alunos
as
	SELECT na.rg + ' ' + na.orgao_exp AS num_documento, nome, titulo as titulo_palestra, nome_palestrante, carga_horaria, data
	FROM Nao_Alunos na 
	INNER JOIN Nao_Alunos_Inscritos n_alu_i ON na.rg = n_alu_i.rg AND na.orgao_exp = n_alu_i.orgao_exp 
	INNER JOIN Palestra pa ON pa.codigo_palestra = n_alu_i.codigo_palestra
	INNER JOIN Palestrante pales ON pales.codigo_palestrante = pa.codigo_palestrante

	CREATE VIEW V_Lista_De_Presen�a
	as
		SELECT * FROM V_Lista_De_Presen�a__Nao_Alunos
		UNION ALL
		SELECT * FROM V_Lista_De_Presen�a_Alunos
	
	SELECT * FROM V_Lista_De_Presen�a ORDER BY nome