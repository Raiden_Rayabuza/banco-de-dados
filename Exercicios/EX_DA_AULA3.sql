/*Fun��es de Valida��o*/
--Verifica se CPF � igual
CREATE PROCEDURE sp_cpf_iguais(@cpf CHAR(11), @bit_cpf_igual BIT OUTPUT)
AS
DECLARE @count INT,
		@char_igual INT
SET @count = 1
SET @char_igual = 0
WHILE(@count <= 11)
	BEGIN
		IF(SUBSTRING(@cpf,1,1) = SUBSTRING(@cpf,@count,1))
			BEGIN
				SET @char_igual += 1
			END
		SET @count += 1
	END
IF(@char_igual = 11)
	BEGIN
		SET @bit_cpf_igual = 1
	END
ELSE
	BEGIN
		SET @bit_cpf_igual = 0
	END
--Verifica se CPF � valido
CREATE PROCEDURE sp_cpf_valido(@cpf CHAR(11), @bit_cpf_invalido BIT OUTPUT)
AS
DECLARE @valores_fixos INT,
		@total INT,
		@primeiro_digito INT,
		@segundo_digito INT,
		@count INT

SET @valores_fixos = 10
SET @total = 0
SET @primeiro_digito = 0
SET @segundo_digito = 0
SET @count = 1
--Calculo do primeiro digito
WHILE (@count < 10)
	BEGIN
		SET @total += CAST(SUBSTRING(@cpf,@count,1) AS INT) * @valores_fixos
		SET @valores_fixos -= 1
		SET @count += 1			
	END
IF(@total % 11 < 2)
	BEGIN
		SET @primeiro_digito = 0
	END
ELSE
	BEGIN
		SET @primeiro_digito = 11 - (@total % 11)
	END
--Calculo do segundo digito
SET @count = 1
SET @valores_fixos = 11
SET @total = 0
WHILE (@count < 11)
	BEGIN
		SET @total += CAST(SUBSTRING(@cpf,@count,1) AS INT) * @valores_fixos
		SET @valores_fixos -= 1
		SET @count += 1		
	END
IF(@total % 11 < 2)
	BEGIN
		SET @segundo_digito = 0
	END
ELSE
	BEGIN
		SET @segundo_digito = 11 - (@total % 11)
	END
IF(@primeiro_digito = CAST(SUBSTRING(@cpf,10,1) AS INT))
	BEGIN
		IF(@segundo_digito = CAST(SUBSTRING(@cpf,11,1) AS INT))
			BEGIN
				SET @bit_cpf_invalido = 0
			END
		ELSE
			BEGIN
				SET @bit_cpf_invalido = 1
			END
	END
ELSE
	BEGIN
		SET @bit_cpf_invalido = 1
	END

/*Principal*/
CREATE PROCEDURE sp_cpf(@cpf CHAR(11), @saida VARCHAR(200) OUTPUT)
AS
DECLARE @e1 BIT,
		@e2 BIT
SET @e1 = 0
SET @e2 = 0
EXEC sp_cpf_iguais @cpf, @e1 OUTPUT
IF(@e1 = 1)
	BEGIN
		RAISERROR('O CPF n�o pode conter apenas n�meros repet�dos',16,1)
	END
ELSE
	BEGIN
		EXEC sp_cpf_valido @cpf, @e2 OUTPUT
		IF(@e2 = 1)
			BEGIN
				RAISERROR('O CPF inserido � invalido',16,1)
			END
		ELSE
			BEGIN
				PRINT('O CPF ' + @cpf + ' � valido')
			END
	END
/*Chamadas de fun��o*/
DECLARE @cpf_user CHAR(11)
SET @cpf_user = '40772054096'
DECLARE @saida_cpf VARCHAR(200)
EXEC sp_cpf @cpf_user, @saida_cpf
PRINT(@saida_cpf)