--COMANDOS PARA CRIAR E USAR O BANCO
CREATE DATABASE Livraria
USE Livraria
--TABELAS
CREATE TABLE Livro(
	Codigo_Livro	INT		IDENTITY(1001,1),
	Nome			VARCHAR(100),
	Lingua			VARCHAR(100),
	Ano				INT,
	PRIMARY KEY(Codigo_Livro)		
);
CREATE TABLE Edicoes(
	ISBN			INT,
	Pre�o			DECIMAL(7,2),
	Ano				INT,
	Num_Paginas		INT,
	QTD_Estoque		INT,
	PRIMARY KEY(ISBN)
);
CREATE TABLE Editoras(
	Codigo_Editoras	INT		IDENTITY(1,1),
	Nome			VARCHAR(50),
	Logradouro		VARCHAR(255),
	Numero			INT,
	CEP				CHAR(8),
	Telefone		CHAR(11),
	PRIMARY KEY(Codigo_Editoras)
);
CREATE TABLE Autores(
	Codigo_Autor	INT		IDENTITY(10001,1),
	Nome			VARCHAR(100),
	Nascimento		DATE,
	Pais			VARCHAR(50),
	Biografia		VARCHAR(MAX),
	PRIMARY KEY(Codigo_Autor)
);
--ASSOCIATIVAS
CREATE TABLE Livro_Edicoes_Editoras(
	EdicoesISBN				INT		NOT NULL	FOREIGN KEY REFERENCES Edicoes(ISBN),
	EditoraCodigo_Editora	INT		NOT NULL	FOREIGN KEY REFERENCES Editoras(Codigo_Editoras),
	LivroCodigo_Livro		INT		NOT NULL	FOREIGN KEY REFERENCES Livro(Codigo_Livro),
);

CREATE TABLE Livro_Autor(
	AutorCodigo_Autor		INT		NOT NULL	FOREIGN KEY REFERENCES Autores(Codigo_Autor),
	LivroCodigo_Livro		INT		NOT NULL	FOREIGN KEY REFERENCES Livro(Codigo_Livro),
);

--1)Modificar o nome da coluna ano da tabela edicoes, para AnoEdicao
SP_RENAME 'Edicoes.Ano','AnoEdicao','COLUMN'; -- Usar apenas se o banco n�o possuir procedimentos ou scripts
--2)Modificar o tamanho do varchar do Nome da editora de 50 para 30
ALTER TABLE Editoras
 ALTER COLUMN Nome VARCHAR(30);
--3)Modificar o tipo da coluna ano da tabela autor para int
--N�o da para converter formatos numericos para formatos de data diretamente
ALTER TABLE Autores
 ALTER COLUMN Nascimento VARCHAR(10);
ALTER TABLE Autores
 ALTER COLUMN Nascimento INT;
--4)Inserir os dados
INSERT INTO Livro (Nome, Lingua, Ano) VALUES ('CCNA 4.1','PT-BR',2015)
INSERT INTO Livro (Nome, Lingua, Ano) VALUES ('HTML 5','PT-BR',2017)
INSERT INTO Livro (Nome, Lingua, Ano) VALUES ('Redes de Computadores','EN',2010)
INSERT INTO Livro (Nome, Lingua, Ano) VALUES ('Android em a��o','PT-BR',2018)

INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('In�cio da Silva',1975,'Brasil','Programador WEB desde 1995')
INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('Andrew Tannenbaum',1944,'EUA','Chefe do Departamento de Sistemas de Computa��o da Universidade de Vrij')
INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('Luis Rocha',1967,'Brasil','Programador Mobile desde 2000')
INSERT INTO Autores (Nome, Nascimento, Pais, Biografia) VALUES ('David Halliday',1916,'EUA','F�sico PH.D desde 1941')

INSERT INTO Livro_Autor (AutorCodigo_Autor, LivroCodigo_Livro) VALUES (10001,1001)
INSERT INTO Livro_Autor (AutorCodigo_Autor, LivroCodigo_Livro) VALUES (10002,1002)
INSERT INTO Livro_Autor (AutorCodigo_Autor, LivroCodigo_Livro) VALUES (10003,1003)
INSERT INTO Livro_Autor (AutorCodigo_Autor, LivroCodigo_Livro) VALUES (10004,1004)

INSERT INTO Edicoes (ISBN, Pre�o, Ano, Num_Paginas, QTD_Estoque) VALUES (0130661023,189.90,2018,653,10)


--5)A universidade do Prof. Tannenbaum chama-se Vrije e n�o Vrij, modificar
UPDATE Autores SET Biografia = 'Chefe do Departamento de Sistemas de Computa��o da Universidade de Vrije' WHERE Codigo_Autor = 10002
--6)A livraria vendeu 2 unidades do livro 0130661023, atualizar
UPDATE Edicoes SET QTD_Estoque = 8 WHERE ISBN = 0130661023
--7)Por n�o ter mais livros do David Halliday, apagar o autor.
DELETE FROM Livro_Autor WHERE AutorCodigo_Autor = 10004
DELETE FROM Autores WHERE Codigo_Autor = 10004